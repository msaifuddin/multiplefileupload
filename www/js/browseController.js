angular.module('browse.controller', [])

.controller('BrowseCtrl', function($scope, $ionicPopup, $timeout) {
    var dialogPopup = null;
    $scope.ufiles = [{file : {name: 'File:001-2929292.jpg'}}, {file : {name: 'File:001-99999999.jpg'}}];
    var file_unique_key;
    var chromeCount = 1;
    
     $scope.openGallery = function () {
        dialogPopup.close();
        try {
            navigator.camera.getPicture(prepareUpload, onGetPictureFail, {
                quality: 50,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY
            });
        } catch (e) {
            console.log("Error in openGallery " + JSON.stringify(e));
        }
    }

    $scope.captureImage = function () {
        dialogPopup.close();
        try {
            navigator.camera.getPicture(prepareUpload, onGetPictureFail, {
                quality: 30,
                destinationType: Camera.DestinationType.FILE_URI
            });

        } catch (e) {
            console.log("Error in CaptureImage " + JSON.stringify(e));
        }
    }

    function prepareUpload(imageURI) {
        console.log("uploadPhoto imageURI " + imageURI);

        window.resolveLocalFileSystemURL(imageURI,
            function (fileEntry) {
                console.log("got image file entry: " + fileEntry.fullPath);
                console.log("FileEntry.toInternalURL() " + fileEntry.toInternalURL());
                imageURI = fileEntry.toInternalURL();
            },
            function () { //error
            }
        );
        /* http://stackoverflow.com/a/21839710/388053
         * a hack to resolve FILE URL issue
         * in Android 5.1
         */
        if (imageURI.substring(0, 21) == "content://com.android") {
            var photo_split = imageURI.split("%3A");
            imageURI = "content://media/external/images/media/" + photo_split[1];
        }
        file_unique_key = "File:00" + chromeCount + "-";

        var fileName = file_unique_key + imageURI.substr(imageURI.lastIndexOf('/') + 1);

        var attachedFileObj = {};
        attachedFileObj.name = fileName;
        attachedFileObj.url = imageURI;

        var fileObj = {};
        fileObj.id = file_unique_key;
        fileObj.file = attachedFileObj;

        $scope.ufiles.push(fileObj);

        chromeCount = chromeCount + 1;

        $scope.$apply();

    }
    
    function onGetPictureFail(message) {
        console.log('onGetPictureFail Error: ' + message);
    }
    
    $scope.upload = function () {
        
        var uploadFile = function (record_id){
        
            if ($scope.ufiles.length == 0) {
                return;
            }
            var fileObject = $scope.ufiles.pop();
            var imageURI = fileObject.file.url;
            var options = new FileUploadOptions();
            options.fileKey = "file";
            options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1) + '.png';
            options.mimeType = "text/plain";

            var win = function (data) {
                console.log("Code = " + data.responseCode);
                console.log("Response = " + data.response);
                console.log("Sent = " + data.bytesSent);
                
                uploadFile(record_id); // call again for next upload.
            };

            var fail = function (error) {
                console.log("upload error " + JSON.stringify(error));
            };

            var params = new Object();
            params.recordId = "001";
            options.params = params;

            var ft = new FileTransfer();
            ft.onprogress = function (progressEvent) {
                if (progressEvent.lengthComputable) {
                    console.log("progress" + progressEvent.total + " " + parseInt(100.0 * progressEvent.loaded / progressEvent.total) + "%");
                }
            }
            ft.upload(imageURI, encodeURI(baseURL + "AddCaseAttachment"), win, fail, options);

        }
        
        uploadFile(recordid);
    }
    
    $scope.removeAttachment = function (id) {
        console.log("removeAttachment id " + id + " ufiles " + JSON.stringify($scope.ufiles));

        for (i = 0; i < $scope.ufiles.length; i++) {
            if ($scope.ufiles[i].id == id) {
                console.log("id record found " + id);
                $scope.ufiles.splice(i, 1);
            }
        }
        console.log("removeAttachment after delete ufiles " + JSON.stringify($scope.ufiles));

    }   
    /* dialog for attachment options */
    $scope.showDialog = function () {
        dialogPopup = $ionicPopup.confirm({

            template: '<div class="button-bar"><a class="button icon-left ion-camera" ng-click="captureImage()">Camera</a><a class="button icon-left ion-document"ng-click="openGallery()">Gallery</a></div>',
            title: "Attachment",
            scope: $scope,
            buttons: [{
                text: "Cancel",
            }]
        });

        dialogPopup.then(function (res) {
            console.log('Tapped!', res);
        });
    };
});